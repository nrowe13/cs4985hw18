﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ErrorTest.aspx.cs" Inherits="ErrorTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Ch20: Halloween Store</title>
    <link href="Styles/Main.css" rel="stylesheet" />
</head>
<body>
    <header><img src="Images/banner.jpg" alt="Halloween Store" /></header>
    <section>
        <form id="form1" runat="server">
            <h1>Error Testing</h1>
            <br/>
            <asp:Button ID="btnGenerateException" runat="server" Text="Generate Exception" CssClass="button" OnClick="btnGenerateException_Click" Width="150px" /><asp:Button ID="btnBrokenLink" runat="server" Text="Broken Link" CssClass="button" OnClick="btnBrokenLink_Click" Width="150px" />
        </form>
    </section>
</body>
</html>
